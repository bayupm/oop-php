<?php

require_once('animal.php');
require('frog.php');
require('ape.php');

// Realase 0
$animal = new Animal("Shaun");
echo "<b>Realase 0</b></br>";
echo "Nama Binatang : $animal->name <br>";
echo "Kaki Binatang : $animal->legs <br>";
echo "Cold_blooded : $animal->cold_blooded <br><br>";

// echo $sheep->name; // "shaun"
// echo $sheep->legs; // 2
// echo $sheep->cold_blooded; // false

// Realase 1

$kodok = new Frog("Buduk");
echo "<b>Realase 1</b></br>";
echo "Nama Katak : $kodok->name <br>";
echo "Kaki Katak  : $kodok->legs <br>";
echo "Jump : " .$kodok->jump(). '<br><br>';

// $sungokong = new Ape("kera sakti");
// $sungokong->yell(); // "Auooo"

// Realase 2

$ape = new Ape("Sungokong");
echo "<b>Realase 2</b></br>";
echo "Nama Ape : $ape->name <br>";
echo "Kaki Ape  : $ape->legs <br>";
echo "Yell : " .$ape->yell(). '<br>';

// $kodok = new Frog("buduk");
// $kodok->jump() ; // "hop hop"


?>