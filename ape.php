<?php

require_once('animal.php');

class Ape extends Animal{
    public function __construct($name, $legs = 2){
        parent::__construct($name, $legs);
    }

    public function yell(){
        return 'Auooo';
    }

}

?>